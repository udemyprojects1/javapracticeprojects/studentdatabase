package StudentDatabase;

import java.util.Scanner;

public class Student {

    private String firstName;

    private String lastName;

    private int gradeYear;

    private int studentID;

    private String courses = "";

    private int tuitionBalance = 0;

    private static int costOfCourse = 600;

    private static int id = 1000;

    //Constructor
    public Student() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter student first name:");
        this.firstName = in.nextLine();

        System.out.print("Enter student last name:");
        this.lastName = in.nextLine();

        System.out.println("1 - Freshman\n2 - Sophmore\n3 - Junior\n4 - Senior\nEnter student class level:");
        this.gradeYear = Integer.parseInt(in.nextLine());

        setStudentID();

    }

    //Generate ID
    private void setStudentID() {
        id++;
        //Grade level + ID
        this.studentID = Integer.parseInt(gradeYear + "" + id);
    }

    //Enroll to courses
    public void enroll() {
        //Get inside a loop, user hits 0 to quit
        do {
            System.out.print("Enter course to enroll (q to quit): ");
            Scanner in = new Scanner(System.in);
            String course = in.nextLine();
            if (!course.equalsIgnoreCase("q")) {
                courses = courses + "\n  " + course;
                tuitionBalance += costOfCourse;
            } else {
                break;
            }
        } while (1 != 0);

    }

    //View balance
    public void viewBalance() {
        System.out.println("Your balance is: " + tuitionBalance + " \u20ac"); //€ sign - \u20ac
    }

    //Pay tuition
    public void payTuition() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your payment, \u20ac: ");
        int payment = scanner.nextInt();
        tuitionBalance -= payment;
        System.out.println("Thank you for payment of: " + payment + " \u20ac");
        viewBalance();
    }

    //Show Status
    public String toString() {
        return "Name: " + firstName + " " + lastName +
                "\nStudent ID: " + studentID +
                "\nGrade level: " + gradeYear +
                "\nCourses enroled: " + courses +
                "\nBalance: " + tuitionBalance + " \u20ac";
    }

}
